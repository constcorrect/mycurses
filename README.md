# mycurses

C++ wrapper for the [ncurses](https://www.gnu.org/software/ncurses/ncurses.html) library using Modules TS.

Note: this is a work-in-progress, API _will_ change.

## Goals

- provide a clean C++ API for using ncurses
- replace manual memory management with RAII
- make API more readable (ncurses uses a lot of unclear abbreviations)
- provide both exceptions and return codes to handle errors
- support modern C++ classes such as string_view

## Status
At the moment most methods just call ncurses functions. Most of them still need
to be renamed and to pass error codes to the clients.
