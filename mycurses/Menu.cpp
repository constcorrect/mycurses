#ifndef __cpp_modules
#include <mycurses/Menu.mpp>
#endif

#include <menu.h>

#ifndef __cpp_lib_modules
#include <stdexcept>
#endif

#ifdef __cpp_modules
module mycurses.menu;

#ifdef __cpp_lib_modules
import std.core;
#endif

import mycurses.window;
#endif

using std::invalid_argument;
using std::logic_error;
using std::runtime_error;

// TODO move to header file
namespace ErrorExplanation {

constexpr char const SYSTEM_ERROR[] = "System error occurred (see errno).";
constexpr char const BAD_ARGUMENT[] =
    "Routine detected an incorrect or out-of-range argument.";
constexpr char const POSTED[] = "The menu has already been posted.";
constexpr char const BAD_STATE[] =
    "Routine was called from an initialization or termination function.";
constexpr char const NO_ROOM[] = "Menu is too large forts  window.";
constexpr char const NOT_POSTED[] = "The menu has not been posted.";
constexpr char const NOT_CONNECTED[] = "No items are connected to the menu.";

} // namespace ErrorExplanation

void processReturnCode(int const code) {
    using namespace ErrorExplanation;
    if constexpr (useExceptions) {
        switch (code) {
        case E_OK:
            return;
        case E_SYSTEM_ERROR:
            throw runtime_error(SYSTEM_ERROR);
        case E_BAD_ARGUMENT:
            throw invalid_argument(BAD_ARGUMENT);
        case E_POSTED:
            throw logic_error(POSTED);
        case E_BAD_STATE:
            throw logic_error(BAD_STATE);
        case E_NO_ROOM:
            throw runtime_error(NO_ROOM);
        case E_NOT_POSTED:
            throw logic_error(NOT_POSTED);
        case E_NOT_CONNECTED:
            throw logic_error(NOT_CONNECTED);
        }
    }
}

namespace ncurses {

MenuItems::~MenuItems() {
    for (ITEM* item : items) {
        free_item(item);
    }
}
void MenuItems::addItem(char const* name, char const* description) {
    ITEM* item = new_item(name, description);
    if (item == nullptr) {
        if constexpr (useExceptions) {
            throw std::runtime_error("Could not create item.");
        }
    }
    items.push_back(item);
}
void MenuItems::commit() { items.push_back(nullptr); }
auto MenuItems::data() { return items.data(); }

void Menu::post() {
    auto code = post_menu(menu);
    processReturnCode(code);
    posted = true;
}

void Menu::setWin(Window& win) {
    WINDOW* menu_win = internal::getWin(win);
    set_menu_win(menu, menu_win);
}

void Menu::setSubWin(Window& win) {
    WINDOW* menu_win = internal::getWin(win);
    set_menu_sub(menu, menu_win);
}

void Menu::setFormat(int rows, int cols) { set_menu_format(menu, rows, cols); }
void Menu::driver(int c) { menu_driver(menu, c); }
void Menu::setMark(char const* mark) { set_menu_mark(menu, mark); }

void Menu::unpost() {
    auto code = unpost_menu(menu);
    processReturnCode(code);
    posted = false;
}

bool Menu::isPosted() const { return posted; }

Menu::~Menu() { free_menu(menu); }

void Menu::setItems(MenuItems&& newItems) {
    items = std::move(newItems);
    auto code = set_menu_items(menu, items.data());
    processReturnCode(code);
}

} // namespace ncurses
