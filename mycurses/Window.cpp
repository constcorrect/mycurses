#ifndef __cpp_modules
#include <mycurses/Window.mpp>
#endif

#include <ncurses.h>

#ifndef __cpp_lib_modules
#endif

#ifdef __cpp_modules
module mycurses.window;

#ifdef __cpp_lib_modules
import std.core;
#endif

#endif

namespace ncurses {

Window::Window(Size size, Position begin)
    : win{::newwin(size.y, size.x, begin.y, begin.x)} {}

Window::~Window() { ::delwin(win); }

Window Window::derive(Size size, Position begin) {
    WINDOW* w = ::derwin(win, size.y, size.x, begin.y, begin.x);
    return Window{w};
}

void Window::addString(const char* str, int n) { ::waddnstr(win, str, n); }

void Window::addString(const std::string& str, int n) {
    this->addString(str.c_str(), n);
}

void Window::addString(std::string_view view) {
    ::waddnstr(win, view.data(), view.size());
}

void Window::addString(Position pos, const char* str, int n) {
    this->move(pos.y, pos.x);
    ::waddnstr(win, str, n);
}

void Window::addchar(const chtype ch) { ::waddch(win, ch); }

void Window::addchar(Position pos, const chtype ch) {
    mvwaddch(win, pos.y, pos.x, ch);
}

void Window::refresh() { ::wrefresh(win); }

void Window::wnoutrefresh() { ::wnoutrefresh(win); }

void Window::doupdate() { ::doupdate(); }

void Window::erase() { ::werase(win); }

void Window::clear() { ::wclear(win); }

void Window::move(int y, int x) { ::wmove(win, y, x); }

int Window::getchar() { return ::wgetch(win); }

Position Window::yx() {
    int y, x;
    getyx(win, y, x);
    return Position{y, x};
}

Position Window::paryx() {
    int y, x;
    getparyx(win, y, x);
    return Position{y, x};
}

Position Window::begyx() {
    int y, x;
    getbegyx(win, y, x);
    return Position{y, x};
}

Position Window::maxyx() {
    int y, x;
    getmaxyx(win, y, x);
    return Position{y, x};
}

void Window::drawBox(chtype verch, chtype horch) { ::box(win, verch, horch); }

void Window::drawHorizontalLine(chtype ch, int n) { ::whline(win, ch, n); }
} // namespace ncurses
