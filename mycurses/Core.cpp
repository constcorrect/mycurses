#ifndef __cpp_modules
#include <mycurses/Core.mpp>
#endif

#include <ncurses.h>

#ifndef __cpp_lib_modules
#include <stdexcept>
#endif

#ifdef __cpp_modules
module mycurses.core;

#ifdef __cpp_lib_modules
import std.core;
#endif

#endif

bool initialized = false;

namespace ncurses {

void initscr() {
    if (!initialized) {
        ::initscr();
        initialized = true;
    }
}

void raw(bool const enable) {
    if (enable) {
        ::raw();
    } else {
        ::noraw();
    }
}

void echo(bool const enable) {
    if (enable) {
        ::echo();
    } else {
        ::noecho();
    }
}

void cbreak(bool const enable) {
    if (enable) {
        ::cbreak();
    } else {
        ::nocbreak();
    }
}

void curs_set(int i) {
    auto result = ::curs_set(i);
    if (result == ERR) {
        throw std::runtime_error(
            "Terminal does not support the requested visibility level.");
    }
}

void clear() { ::clear(); }

void start_color() { ::start_color(); }

void endwin() { ::endwin(); }

} // namespace ncurses
